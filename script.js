import { firebaseConfig, app, db } from "./module.js";
import { getDatabase, ref, set, get, child, update, remove } from "./module.js";

console.log(db);
let namebox = document.getElementById("nameBox");
let rollbox = document.getElementById("rollBox");
let sectionbox = document.getElementById("sectionBox");
let generatebox = document.getElementById("generateBox");
console.log(rollbox);

let insBtn = document.getElementById("insBtn");
let selBtn = document.getElementById("selBtn");
let updBtn = document.getElementById("updBtn");
let delBtn = document.getElementById("delBtn");

function insertData() {
  console.log(rollbox.value + "ROLL VAl")
  // rollbox.value !== '' ? console.log('good') : console.log("bad")
  if (rollbox.value === '') {
    alert("You're wrong!")
    return
  }
  set(ref(db, "theStudentss/" + rollbox.value), {
    studentName: namebox.value,
    numOfRoll: rollbox.value,
    section: sectionbox.value,
    generate: generatebox.value,
  }).then(() => {
    alert("Data have inserted");
    console.log("Successfully inserted!");
    namebox.value = '';
    rollbox.value = '';
    sectionbox.value = '';
    generatebox.value = '';
  })
    .catch((err) => console.log("Oops, it's error time", err));
}

function selectData() {
  const dbref = ref(db);

  get(child(dbref, "theStudents/" + rollbox.value))
    .then((snapshot) => {
      if (snapshot.exists()) {
        namebox.value = snapshot.val().studentName;
        rollbox.value = snapshot.val().numOfRoll;
        sectionbox.value = snapshot.val().section;
        generatebox.value = snapshot.val().generate;
        alert("Data have succussfully selected!");
      } else {
        alert("No data found...");
      }
    })
    .catch((err) => console.log("Opps, it's error while selecting", err));
}

function updateData() {
  update(ref(db, "theStudents/" + rollbox.value), {
    studentName: namebox.value,
    numOfRoll: rollbox.value,
    section: sectionbox.value,
    generate: generatebox.value,
  })
    .then(() => alert("Data have succussfully updated!"))
    .catch((err) => console.log("Opps, it's error while updating", err));
}

function deleteData() {
  remove(ref(db, "theStudents/" + rollbox.value))
    .then(() => alert("Data have successfully removed"))
    .catch((err) => console.log("Oops, deletion error", err));
}

insBtn.addEventListener("click", insertData);
selBtn.addEventListener("click", selectData);
updBtn.addEventListener("click", updateData);
delBtn.addEventListener("click", deleteData);
insBtn.addEventListener("click", console.log("dfsadf"));