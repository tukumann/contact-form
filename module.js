import {
  getDatabase,
  ref,
  set,
  get,
  child,
  update,
  remove,
} from "https://www.gstatic.com/firebasejs/9.8.2/firebase-database.js";

// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyDl0rIAKFhC-kFVha-Brt4w1RJRRr9aVmM",
  authDomain: "angular-firebase-site.firebaseapp.com",
  databaseURL:
    "https://angular-firebase-site-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "angular-firebase-site",
  storageBucket: "angular-firebase-site.appspot.com",
  messagingSenderId: "939512389176",
  appId: "1:939512389176:web:f1e8a8e6af8d844e62d0cd",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);

export const db = getDatabase();

export { getDatabase, ref, set, get, child, update, remove };